<%@ include file="/WEB-INF/layouts/include.jsp" %>
<h1>Oh Really</h1>
<form>
	<div class="mb-3">
		<label for="carpartId" class="form-label">Car Part Id</label>
		<orly-input id="partId" url="<c:url value='/transaction/getTransactions'/>" 
		value="partId" labelproperty="partId" idproperty="partId" placeholder="Part Ids">
	</div>
	<div class="mb-3">
		<label for="saleDate" class="form-label">Sale Date</label>
		<orly-datepicker id="saleDate" disabled name="saleDate"></orly-datepicker>
	</div>
	<div class="mb-3">
		<label for="tendered" class="form-label">Tendered</label>
		<input type="text" id="tendered" name="tendered" class="form-control">
	</div>
	<div class="mb-3">
		<label for="change" class="form-label">Change</label>
		<input type="text" id="change" name="change" disabled class="form-control">
	</div>
	<div class="mb-3">
		<button type="button" class="btn btn-primary" id="submitBtn">Submit</button>
	</div>
</form>
<orly-table id="transactionTable" loaddataoncreate url="<c:url value='/transaction/getTransactions' />"
				includefilter bordered maxrows="10" tabletitle="Transactions" class="invisible">
	<orly-column field="txId" label="Transaction ID" sorttype="natural"></orly-column>
	<orly-column field="partId" label="Part ID"></orly-column>
	<orly-column field="saleDate" label="Sale Date"></orly-column>
	<orly-column field="tendered" label="Tendered"></orly-column>
	<orly-column field="change" label="Change"></orly-column>
</orly-table>
<script>
	orly.ready.then(() => {
		setSaleDate();
		getChangeEvent();
		submitForm();
		// add ajax for select options
	});
	 
	function setSaleDate() {
		let saleDate = orly.qid("saleDate");
		saleDate.value = new Date();
	}

	function submitForm() {
		orly.qid("submitBtn").addEventListener("click", function(e){ 
			try {
				
				let transactionData = {};
				transactionData.partId = orly.qid("partId").value;
				transactionData.saleDate = orly.qid("saleDate").sqlTimestamp;
				transactionData.tendered = orly.qid("tendered").value;
				transactionData.change = orly.qid("change").value;
				
				fetch("<c:url value='/transaction/postTransactionDetails'/>", {
					method: "POST", 
					body: JSON.stringify(transactionData),
					headers: {
						"Content-Type":"application/json"
					}
				}).then(function(res){
					if(res.ok) {
						return res.json();
					} else { 
						console.error(res.status);
					}
				}).then(function(Response) {
					console.log(Response);
					let message = Response.message;
					let messageType = Response.messageType;
					let duration = 10000;
					orly.qid('alerts').createAlert({'msg': message, 'type': messageType, 'duration': duration});
					//orly.qid("transactionTable").loadData(); 
				});
				
			} catch(err) {
				console.error(err);
			}
		});
	}
	
	function getChangeEvent() {
		var tendered = orly.qid("tendered");
		tendered.addEventListener("change", function(e){
			try {
				fetch("<c:url value='/transaction/getPartDetails' />")
				.then(function(response){
					if(response.ok){
						console.log("ok");
						return response.json();
					} else {
						throw new Error(getError(response));
					}
				}).then(function(json){
					if(json != null && json != "undefined"){
					 	var partId = orly.qid("partId").value; 
						console.log("we good", json.partDetails);
						for(let i = 0; i < json.partDetails.length; i++) {
							if(partId == json.partDetails[i].partId) {
								let price = json.partDetails[i].price; 
								let sum = (e.target.value - price);
								orly.qid("change").value = sum;
							}
						}
					} else {
						orly.qid("alert").createAlert({'msg': "json is undefined", 'type': "danger", 'duration': 3000});
					}
				}).catch(function(err){
					console.log("Internal error in the fetch: ", err);
				});
			} catch(error) {
				console.log("External Fetch Error: ", error);
			}
		});
	}
</script>