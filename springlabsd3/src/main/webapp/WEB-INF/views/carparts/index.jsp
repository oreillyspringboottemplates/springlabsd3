<%@ include file="/WEB-INF/layouts/include.jsp"%>

<%-- 
===================================== 
       VERSION 1 of the JSP 
=====================================
--%>
<!-- <h1>Car Parts</h1>
<div class="card">
	<div class="card-body">
		<p>My Car Parts Page</p>
	</div>
</div> -->


<%-- 
===================================== 
       VERSION 2 of the JSP 
=====================================
--%>
<%-- <h1>Car Parts</h1>
<h2>Welcome, 
	<c:choose>
		<c:when test="${not empty fullName}">${fullName}</c:when>
		<c:otherwise>Guest</c:otherwise>
	</c:choose>
</h2>
<div class="card">
	<div class="card-body">
		<p>My Car Parts Page</p>
	</div>
</div> --%>

<%-- 
===================================== 
       VERSION 3 of the JSP 
=====================================
--%>
<div>
	<h1>Car Parts - 1994 Pontiac Firebird</h1>
	<div class="row">
		<div class="col-sm-6">
			<b>Part Number:</b> ${carpart.partNumber}<br/>
			<b>Line:</b> ${carpart.line}<br/>
			<b>Description:</b> ${carpart.description}<br/><br/>
			<b>DETAILS</b><br/>
		</div>
		<div class="col-sm-3">
			<c:if test="${not empty carpart.imageName}">
				<img class="img-fluid" src="<c:url value='/resources/img/carparts/${carpart.imageName}'/>" />
			</c:if>
		</div>
	</div>	
</div>
