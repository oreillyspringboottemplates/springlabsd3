package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd3Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd3Application.class, args);
	}

}
